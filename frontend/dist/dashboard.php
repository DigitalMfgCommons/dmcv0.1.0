<!doctype html> <html class=no-js> <head> <meta charset=utf-8> <meta name=description> <meta name=viewport content="width=device-width, initial-scale=1"> <title>Dashboard</title> </head> <body ng-app=dmc.dashboard ng-controller=DashboardController> <!--[if lt IE 10]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->  <div dmc-top-header show-notification=true active-page="'dashboard'"></div> <div ui-view></div> <dmc-footer></dmc-footer> <?php include 'build-vendor.php' ?> <script src=scripts/dashboard/index.js></script> <script type=text/javascript>
        <?php
            if (isset($_SERVER['AJP_givenName'])) {
                echo('window.givenName = "'.$_SERVER['AJP_givenName'].'";');
            } else {
                echo('window.givenName = "";');
            }
        ?>
        window.apiUrl = '';
    </script> </body> </html> 