<!doctype html> <html class=no-js> <head> <meta charset=utf-8> <meta name=description> <meta name=viewport content="width=device-width, initial-scale=1"> <title>DMDII Member Page</title>  </head> <body ng-app=dmc.member> <!--[if lt IE 10]>
 <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->  <div dmc-top-header members-only=true active-page="'members&memberPage'"></div> <div ui-view></div> <dmc-footer></dmc-footer> <?php include 'build-vendor.php' ?> <script src=scripts/member-page/index.js></script> <script type=text/javascript>
        <?php
            if (isset($_SERVER['AJP_givenName'])) {
                echo('window.givenName = "'.$_SERVER['AJP_givenName'].'";');
            } else {
                echo('window.givenName = "";');
            }
        ?>
        window.apiUrl = '';
    </script> </body> </html> 